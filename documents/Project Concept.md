#Project Concept Document

![Table](http://s8.postimg.org/wz5qooyzp/Table.png)

#Description 
* A fun mobile gaming application, that educates users on harmful consequences of using plastic bags. The main aim of the game is to make the users realize that due to the endless demands and aspirations of the people the world has been invaded by plastics. The game focuses at educating the users to reduce the plastic usage and enhance the usage of biodegradable substances hence helping in protecting the environment. The game will have a series of maps, with varying conditions which the user has to meet in order to Save the World. The difficulty ramps up at a perfect pace, with smartly designed levels that teach the difficulty of the game as the user play. The one with the less plastic movement on their team wins. 

* The game would be a cross platform gaming application. The game will be available on the mobile devices having Android and IOS operating system.

#Profile of Users 
* The game is being developed keeping in mind the lifestyle of the users. People now a days due to their busy schedule don't find much time for strategy/mind orientated games. “Saving the World” provides a great opportunity for such users. “Saving the World” would be a portable strategy game which will help the users utilize there precious time in learning the consequences of increasing plastic influence in a fun way. 

* The game is basically designed for people above the age of 13 years without any gender bias. The game could be played by anyone having basic knowledge of how to use a smart phone/gadget. The user is expected to have a medically certified proper eye sight, physically functioning arms,forearms,fingers and is adequately mentally equipped. The user should possess a smart phone/gadget  equipped with Android or IOS operating system. The user could wary from a student, to a working professional, to a home-maker, to the elderly. This game at the present stage is only developed for mobile users.

### Student

* A student here refers to a person belonging to the age group of 13-18. Students now a days learn a lot of things through visuals like videos, games etc. “Saving the World” could become a way of teaching young minds about saving the world by using less plastic and increasing the use of Biodegradable substances. This game can help the students learn good habits and by inculcating proper moral values regarding the environment they can teach their peer groups and even their parents about the problems related to plastic use.

### The Working Class

* This class include all the people belonging to the age group of 18-60. The working professionals will include both the direct and indirect contributors to the GDP. The people in this age group are the one who hold the key in improving the future by taking precautionary steps today. The habits or the ideas they develop today by learning through these strategy games will help in formulating policies which would help in saving the environment.

### Elderly

* This class include all people belonging to the age group of above 60. People belonging to this class are the one who guide the young generation with their experience. With their experience we can actually form ideas which would help reduce the non biodegradable usage on the planet.

#Usage Model and Diagram
* The user initially has the option to choose between the following characters:
O - Oliver , L - Linda, P - Paul , B – Bailey(Still in discussion) and M – Mark and a profile is made.
Then the user is taken into a map of level 1 and game play proceeds as normal. The cut scenes will be provided by the client. The user navigates through an urban city landscape, starting from a morning at home and accumulate points depending on his eco friendliness.

![diagram1](http://s15.postimg.org/afzidkx97/Game_Flow.png)
![diagram](http://s1.postimg.org/odua76m67/OLPBM.png)
