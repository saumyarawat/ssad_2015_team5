###Date- September 16,'15

##Attendes
* Anjali Shenoy
* A Ramya
* Dhruv Sapra
* Saumya Rawat
* Sreeja Kamishetty

##Agenda
Give the final shape to the characters- Oliver, Linda, Paul, Boston/Bailey, Mark
###Discussion
* A rough sketch of all the characters was made. Proper hair styles along with the dresses were decided. 
* The compability of Unity and IBM Blue mix was discussed.


###Action Point
The final shape to all the characters and the cut scenes would be given and the conversion of the characters from normal PNG format to unity compatible would be done. Apart from this a sample home screen for the game would be developed.