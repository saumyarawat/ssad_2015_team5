s## Date and time
August 19 2015 9.30 AM.

## Attendees
Saumya Rawat, Anjali Shenoy, Dhruv Sapra, Ramya K, Sreeja Kamishetty

## Agenda
Initial discussion on the game design document

## Minutes
### Discussion
The client, Ms Sandra had a template for the game design document template which we discussed at length
* Basic plot of the game
 * Characters in the game: Minimum 5 Characters, gender of polar bear (the main character)
 * "One Less Plastic Bag Movement (OLPBM)" we can have the Names of the actors in the game like..... O - Oliver , L - Linda, P - Paul , B - Betty and M - Mark
 * Target Audience: Around 13+ , Mainly teenagers
 * Atleast 5 levels
* Mission Challenge structure was discussed
 * Proposals for the scoring system: Bonus meter,Minimum score wins
 * There will be sub characters as well but their outline will be provided by the clients team

### Action Points
* We will be sent the design template that was used by the client shortly
 * Go through the document
 * Make changes and add our own ideas
 * Develop a basic story line along the lines of the client's idea

## Date of the next meeting
  26th August '15 12.00 PM
