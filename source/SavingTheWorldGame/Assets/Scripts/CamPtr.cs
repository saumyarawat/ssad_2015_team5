﻿/*using UnityEngine;
using System.Collections;

public class CamPtr : MonoBehaviour {
	public GameObject trolley;
	private Vector3 offset;
	public Transform target;
	// Use this for initialization
	void Start () {
		offset = transform.position - trolley.transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!target) return;	 
		transform.position = trolley.transform.position + offset;
		transform.LookAt(target);
	
	}
}*/

// Smooth Follow from Standard Assets
// Converted to C# because I fucking hate UnityScript and it's inexistant C# interoperability
// If you have C# code and you want to edit SmoothFollow's vars ingame, use this instead.
using UnityEngine;
using System.Collections;

public class CamPtr : MonoBehaviour
{
	
	// The target we are following
	public Transform target;
	// The distance in the x-z plane to the target
	private float distance = 0.050f;
	// the height we want the camera to be above the target
	private float height = 1.50f;// 1.50f;
	// How much we 
	private float heightDamping = 2.0f;
	private float rotationDamping = 9.0f;
	
	// Place the script in the Camera-Control group in the component menu
	[AddComponentMenu("Camera-Control/Smooth Follow")]
	
	void LateUpdate()
	{
		// Early out if we don't have a target
		if (!target) return;
		
		// Calculate the current rotation angles
		float wantedRotationAngle = target.eulerAngles.y+270;
		float wantedHeight = target.position.y + height;
		
		float currentRotationAngle = transform.eulerAngles.y ;
		float currentHeight = transform.position.y;
		
		// Damp the rotation around the y-axis
		currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
		
		// Damp the height
		currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
		
		// Convert the angle into a rotation
		var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
		
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		transform.position = target.position;
		transform.position -= currentRotation * Vector3.forward * distance;
		
		// Set the height of the camera
		transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
		
		// Always look at the target
		transform.LookAt(target);
	}
}
